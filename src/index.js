import React from "react";
import ReactDOM from "react-dom";

import { Provider as AlertProvider } from "react-alert";

import store from "./redux/store";
import { Provider } from "react-redux";

import App from "./App";

import "./index.css";
import { AlertTemplateNotification } from "./Notification";
import { alertNotificationOptions } from "./Notification/setting";

ReactDOM.render(
  <Provider store={store}>
    <AlertProvider
      template={AlertTemplateNotification}
      {...alertNotificationOptions}
    >
      <App />
    </AlertProvider>
  </Provider>,
  document.getElementById("root")
);

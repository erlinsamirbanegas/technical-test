import React from "react";
import svgs from "./svgs";

import "./icon.scss";

const Icon = ({ svg, classes, click, title }) => {
  const svgRender = svgs[svg] || svgs.default;
  return (
    <svg
      viewBox={svgRender.viewBox}
      className={`icon-svg ${classes}` }
      title={title}
      xmlns="http://www.w3.org/2000/svg"
    >
      {svgRender.svg}
    </svg>
  );
};

export default Icon;

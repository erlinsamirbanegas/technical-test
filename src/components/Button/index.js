import React from "react";
import { Link } from "react-router-dom";

import "./button.scss";

export const Button = ({ link, text, onClick }) => {
  const click = () => {
    if (onClick) return onClick();
  };

  return link ? (
    <Link className="link-button" to={link}>
      <button className="custom-btn btn">
        <span>{text}</span>
      </button>
    </Link>
  ) : (
    <button className="custom-btn btn" onClick={click}>
      <span>{text}</span>
    </button>
  );
};

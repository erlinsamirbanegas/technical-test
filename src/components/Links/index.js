import React from "react";
import { Link } from "react-router-dom";

import "./links.scss";

const Links = () => {
  return (
    <ul id="links">
      <li>
        <Link className="link" to="/contenidos">
          Contenidos
        </Link>
      </li>
      <li>
        <Link className="link" to="/contadores">
          Contadores
        </Link>
      </li>
    </ul>
  );
};

export default Links;

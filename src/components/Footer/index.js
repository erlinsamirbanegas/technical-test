import React from "react";
import Links from "../Links";
import Icon from "../Icon";

import "./footer.scss";

const Footer = () => {
  return (
    <footer id="footer">
      <Links />

      <div className="wrapper_social_media">
        <Icon svg="youtube" classes="icon_media" />
        <Icon svg="facebook" classes="icon_media" />
        <Icon svg="twitter" classes="icon_media" />
      </div>
    </footer>
  );
};

export default Footer;

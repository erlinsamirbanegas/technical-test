import React from "react";
import { Link } from "react-router-dom";

import logo from "../../logo/fz-logo.svg";
import Links from "../Links";

import "./header.scss";

const Header = () => {
  return (
    <div id="header">
      <img src={logo} alt="Logo FZ Sports" />
      

      <Links />
    </div>
  );
};

export default Header;

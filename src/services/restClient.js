const getUrl = () => {
  const env = window.location.host;

  return "http://localhost:3000";
};

export const getRequestUserInfo = () =>
  sessionStorage.requestUserInfo
    ? JSON.parse(sessionStorage.requestUserInfo)
    : null;

export const objectParametize = (obj, q, parent) => {
  const str = [];
  const delimeter = "&";
  let objKey;
  const a = Object.keys(obj);
  a.forEach((key) => {
    switch (typeof obj[key]) {
      case "object":
        if (obj[key]) {
          if (Array.isArray(obj[key])) {
            obj[key].forEach((arrObject) => {
              if (parent) {
                objKey = `${parent}.${key}`;
              } else {
                objKey = key;
              }
              if (String.isString(arrObject) || Number.isNaN(arrObject)) {
                if (parent) {
                  str[str.length] = `${parent}.${key}=${arrObject}`;
                }
                str[str.length] = `${key}=${arrObject}`;
              } else if (!String.isString(arrObject)) {
                str[str.length] = objectParametize(arrObject, false, objKey);
              }
            });
          } else if (Array.isArray(obj[key])) {
            str[str.length] = `${parent}.${key}=${obj[key]}`;
          } else {
            if (parent) {
              objKey = `${parent}.${key}`;
            } else {
              objKey = key;
            }
            str[str.length] = objectParametize(obj[key], false, objKey);
          }
        }
        break;
      default: {
        if (obj[key]) {
          if (parent) {
            str[str.length] = `${parent}.${key}=${obj[key]}`;
          } else {
            str[str.length] = `${key}=${obj[key]}`;
          }
        }
      }
    }
  });

  return (q === true ? "?" : "") + str.join(delimeter);
};

const urlBase = getUrl();

export class restClient {
  static httpGet = (url, obj) => {
    const request = {
      ...obj,
    };
    let urlparam;

    if (request) {
      urlparam = `&${objectParametize(request, false)}`;
    }

    const paramUrl = `${url}?format=json${urlparam}`;

    return fetch(`${urlBase}${paramUrl}`)
      .then((response) => {
        if (response && response.status && response.status === 404) {
          return response;
        }

        let json;

        try {
          json = !response.message && response.json();
        } catch (error) {
          json = { error: "Problema al conectar con el server", ...error };
        }

        return json;
      })
      .then((response) => {
        if (response && response.status && response.status === 404) {
          return response;
        }

        return response;
      })
      .catch((error) => {
        return { error: "Problema al conectar con el server", ...error };
      });
  };

  static httpPost = (url, obj, useWaitControl = true) => {
    const requestUserInfo = getRequestUserInfo();

    const request = {
      ...obj,
      requestUserInfo,
    };

    return fetch(`${urlBase}${url}`, {
      method: "POST",
      body: JSON.stringify(request),
      headers: { "Content-type": "application/json" },
    })
      .catch((error) => {})
      .then((response) => response.json())
      .then((response) => response);
  };

  static httpPut = (url, obj) => {
    const request = {
      ...obj,
    };
    return fetch(`${urlBase}${url}`, {
      method: "PUT",
      body: JSON.stringify(request),
      headers: { "Content-type": "application/json" },
    })
      .catch((error) => {})
      .then((response) => response.json())
      .then((response) => response);
  };

  static httpDelete = (url, obj) => {
    const request = {
      ...obj,
    };

    return fetch(`${urlBase}${url}`, {
      method: "DELETE",
      body: JSON.stringify(request),
      headers: { "Content-type": "application/json" },
    })
      .catch((error) => {})
      .then((response) => response.json())
      .then((response) => response);
  };
}

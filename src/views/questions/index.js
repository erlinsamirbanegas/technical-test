import React, { useMemo } from "react";
import { data } from "./data";

import "./question.scss";
import { QuestionItem } from "./questionItem";

const Questions = () => {
  const questionAsnwers = useMemo(
    () => data.map((item, index) => <QuestionItem key={index} {...item} />),
    []
  );

  return (
    <div className="container-questions">
      <h2>Preguntas Teóricas</h2>

      <div className="wrapper-questions">{questionAsnwers}</div>
    </div>
  );
};

export default Questions;

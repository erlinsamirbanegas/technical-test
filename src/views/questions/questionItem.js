import React from "react";

export const QuestionItem = ({ question, answer, example }) => {
  return (
    <div className="wrap-question">
      <h3>{question}</h3>
      <p>{answer}</p>

      {example && (
        <div className="example-question">
          <textarea>{example}</textarea>
        </div>
      )}
    </div>
  );
};

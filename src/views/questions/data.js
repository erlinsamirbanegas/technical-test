export const data = [
  {
    question:
      "¿Explique la diferencia entre inputs controlados y no controlados ?",
    answer: `Controlados: aceptan su valor como un apoyo, asimismo una devolución de llamada para actualizar ese valor. Este valor es almacenado en el estado del componente.
            No Controlados: Es como trabajar con HTML directamente, el tema se centra en el DOM, donde accedemos a esos valores con una ref, podría ser el onClick controlador del botón, o directamente actualizar ese valor por medio de la ref.
    `,
  },
  {
    question:
      "Cómo prevenir que los componentes vuelvan a ser re-renderizados?",
    answer: `Podemos usar React.Memo o useMemo, además se pueda manejar la data de forma inmutable esto evitaría renderizados inncesarios, el uso de redux evita que los componentes sear renderizados siempre y cuando lo necesiten.`,
  },
  {
    question: "Qué es un componente HOC? Agregar ejemplo:",
    answer: `Es un patrón que se usa especialmente para reutilizar de un componente, para ello se crea una función que recibe como parámetro un componente y retorna uno nuevo decorado.`,
    example: `
import React, { Component } from 'react';

class App extends Component {
 constructor(...props) {
     super(...props)
     this.state = { data: [] }
 }
 componentDidMount() {
    fetch('https://jsonplaceholder.typicode.com/posts')
    .then(response => response.json())
    .then(data => {
      this.setState({ data })
    })
    .catch(err => console.log(err.message))
 }
 
 render() {
    const { data } = this.state
     return (
           <ul>
               {data.map(post =>  <li key={post.id}>{post.title}</li> )}
           </ul>
     )
 }
}

export default App



//Creando HOC

import React,{ Component } from 'react'

export default function withDataFetching(endpoint, WrappedComponent) {
  return class extends Component {
    constructor(props) {
      super(props)
      this.state = { data: [] }
    }

    componentDidMount() {
      fetch(endpoint)
        .then(response => response.json())
        .then(data => {
          this.setState({ data })
        })
        .catch(err => console.log(err.message))
    }

    render() {
      return <WrappedComponent data={this.state.data} {...this.props} />
    }
  }
}


//Implementando HOC

import React,{ Component } from 'react';
import withDataFetching from './withDataFetching'

class App extends Component {
  render() {
    return (
      <ul>
        {this.props.data.map(post =>  <li key={post.id}>{post.title}</li> )}
      </ul>
    )
  }
}

export default withDataFetching('https://jsonplaceholder.typicode.com/posts', App)

    `,
  },
  {
    question: "Qué es React Redux?",
    answer: `Es una capa de enlaces diseñada especialmente de React para Redux, en donde permite a los componentes React leer los datos del estado y acciones para actualizar ese estado Redux.
    Redux por sí nos sirve para manejar una única fuente de verdad, un estado Global de la aplicación.
    `,
  },
  {
    question: "Para que sirven las arrow function en React?",
    answer: `Sirven para crear funciones como alternativa a las funciones tradicionales, fueron introducidas desde ES6, semánticamente son más atractivas, en ellas no se maneja el termino this.
    `,
  },
];

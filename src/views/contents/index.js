import React, { useEffect } from "react";
import { useAlert } from "react-alert";
import { useDispatch, useSelector } from "react-redux";
import { Carousel } from "react-responsive-carousel";

import { Button } from "../../components/Button";
import useScreenType from "../../hooks/useScreenType";

import { setPlaylist } from "../../redux/reducers/playlist";
import { restClient } from "../../services/restClient";

import "./contents.scss";

const Contents = () => {
  const { data } = useSelector((state) => state.playlist);
  const dispatch = useDispatch();

  const type = useScreenType();
  const alert = useAlert();

  useEffect(() => {
    const fetchPlaylistAsync = async () => {
      const response = await restClient.httpGet(
        "/api/generic/playlists/details/5b845b8346cc29000e4f186a",
        { itemsPerPage: 10 }
      );

      if (response.error) {
        alert.error(response.error);

        return;
      }

      dispatch(setPlaylist(response.data.items));
    };

    fetchPlaylistAsync();
  }, [alert, dispatch]);

  const getImageType = (images) => {
    const image = images.find((s) => s.type === type);

    return `https://mychannel.nunchee.tv/api/assets/images/view/${image._id}?type=${image.type}&scale=25&placeholder=true`;
  };

  return (
    <Carousel
      className="carrusel"
      autoPlay
      infiniteLoop
      showThumbs
      centerSlidePercentage={80}
    >
      {data &&
        data.length > 1 &&
        data.map((item, index) => (
          <div key={item._id} className="carrusel-item">
            <img alt="Legend 1" src={getImageType(item.images)} />

            <div className="legend">
              <strong>{item.title.original}</strong>
              <p>Legend 1</p>

              <Button link={`/contenidos/detalle/${item._id}`} text="Ver más" />
            </div>
          </div>
        ))}
    </Carousel>
  );
};

export default Contents;

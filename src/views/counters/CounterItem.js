import React from "react";
import { useDispatch } from "react-redux";
import { setDecrease, setIncrement } from "../../redux/reducers/counters";

import "./counter.scss";

export const CounterItem = ({ counter }) => {
  const dispatch = useDispatch();

  const onDecreaseClick = () => {
    dispatch(setDecrease({ id: counter.id, value: counter.id + 1 }));
  };

  const onIncrementClick = () => {
    dispatch(setIncrement({ id: counter.id, value: counter.id + 1 }));
  };

  return (
    <div key={counter.id} className="counter-card">
      <h4>id: {counter.id}</h4>
      <h2>{counter.value}</h2>
      <div className="actions">
        <button onClick={onDecreaseClick}>-</button>
        <button onClick={onIncrementClick}>+</button>
      </div>
    </div>
  );
};

import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { Button } from "../../components/Button";
import { setCounter } from "../../redux/reducers/counters";

import "./counter.scss";
import { CounterItem } from "./CounterItem";

function* generateId() {
  let i = 0;

  while (true) {
    yield i++;
  }
}

const iterator = generateId();

const Counters = () => {
  const { counters } = useSelector((state) => state.counters);
  const dispatch = useDispatch();

  const addCounter = () => {
    const counter = {
      value: 0,
      id: iterator.next().value,
    };

    dispatch(setCounter(counter));
  };

  const getTotalCounter = () =>
    counters.length ? counters.reduce((a, b) => a + (b.value || 0), 0) : 0;

  return (
    <div id="wrapper-counter">
      <div className="wrapp-actions-total">
        <div className="counter-total">Total: {getTotalCounter()}</div>

        <Button onClick={addCounter} text="Nuevo Contador" />
      </div>

      <div className="wrap-cards">
        {counters.length &&
          counters.map((counter) => (
            <CounterItem key={counter.id} counter={counter} />
          ))}
      </div>
    </div>
  );
};

export default Counters;

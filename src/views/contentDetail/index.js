import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { useParams } from "react-router-dom";

import useScreenType from "../../hooks/useScreenType";
import { setContentDetail } from "../../redux/reducers/playlist";

import { restClient } from "../../services/restClient";

import "./contentDetail.scss";

const ContentDetail = () => {
  const { contentDetail } = useSelector((state) => state.playlist);
  const dispatch = useDispatch();

  const { id } = useParams();
  const type = useScreenType();

  useEffect(() => {
    const fetchContentDetailAsync = async () => {
      const response = await restClient.httpGet(
        `/api/ott/contents/details/${id}`
      );

      if (response.error) {
        alert.error(response.error);

        return;
      }

      dispatch(setContentDetail(response.data));
    };

    fetchContentDetailAsync();
  }, [dispatch, id]);

  const getImageType = (images) => {
    const image = images.find((s) => s.type === type);

    return `https://mychannel.nunchee.tv/api/assets/images/view/${image._id}?type=${image.type}&scale=25&placeholder=true`;
  };

  return (
    <div id="wrapper-content-detail">
      <div className="image-wrapp">
        {Object.keys(contentDetail).length && (
          <img
            src={getImageType(contentDetail.images)}
            alt="Detalle de contenido"
          />
        )}

        {Object.keys(contentDetail).length && (
          <div id="detail">
            <strong>{contentDetail.title.original}</strong>
            <p>{contentDetail.description.plain.original}</p>
          </div>
        )}
      </div>
    </div>
  );
};

export default ContentDetail;

import "react-responsive-carousel/lib/styles/carousel.min.css";

import React from "react";

import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

//Components
import Contents from "./views/contents";
import Header from "./components/Header";
import Main from "./layout/Main";
import Footer from "./components/Footer";

import ContentDetail from "./views/contentDetail";

import "./app.scss";
import Counters from "./views/counters";
import Questions from "./views/questions";

const App = () => {
  return (
    <Router>
      <div id="app">
        <Header>FZ Sports</Header>

        <Main>
          <Switch>
            <Route
              exact
              path="/contenidos/detalle/:id"
              component={ContentDetail}
            />
            <Route exact path="/contenidos" component={Contents} />
            <Route exact path="/contadores" component={Counters} />

            <Route exact path="/" component={Questions} />
          </Switch>
        </Main>

        <Footer />
      </div>
    </Router>
  );
};

export default App;

import { useEffect, useState } from "react";
import useWindowSize from "./useWindowSize";

const useScreenType = () => {
  const [type, setType] = useState('backdrop');

  const { width } = useWindowSize();

  useEffect(() => {
    if (width >= 1200) setType("backdrop");

    if ((width) => 798) setType("mediumh");

    if (width <= 480) setType("square");
  }, [width]);

  return type;
};

export default useScreenType;

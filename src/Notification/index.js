import React from "react";
import Icon from "../components/Icon";

import "./notification.scss";

const getBackgroundColorAlert = (type) => {
  switch (type) {
    case "success":
      return "#3f8159";
    case "info":
      return "#ffc83d";
    case "error":
      return "#d83b01";

    default:
      return "yellow";
  }
};

export const AlertTemplateNotification = ({
  style,
  options,
  message,
  close,
}) => (
  <div
    className="notification-alert"
    style={{ background: getBackgroundColorAlert(options.type) }}
  >
    {options.type === "info" && <Icon svg="info" />}
    {options.type === "success" && <Icon svg="info" />}
    {options.type === "error" && <Icon svg="info" />}
    <span>{message}</span>
    {/* <button onClick={close}>X</button> */}
  </div>
);

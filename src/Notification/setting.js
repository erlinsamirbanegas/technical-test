import { transitions, positions } from "react-alert";

export const alertNotificationOptions = {
  position: positions.TOP_RIGHT,
  timeout: 5000,
  offset: "100px",
  transition: transitions.SCALE,
};

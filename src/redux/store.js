import { configureStore } from "@reduxjs/toolkit";

import paylistReducer from "./reducers/playlist";
import countersReducer from "./reducers/counters";

export default configureStore({
  reducer: {
    playlist: paylistReducer,
    counters: countersReducer
  },
});

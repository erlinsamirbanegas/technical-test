import { createSlice } from "@reduxjs/toolkit";

export const playlistSlice = createSlice({
  name: "playlist",
  initialState: {
    data: [],
    contentDetail: {},
  },
  reducers: {
    setPlaylist: (state, action) => {
      state.data = action.payload;
    },
    setContentDetail: (state, action) => {
      state.contentDetail = action.payload;
    },
  },
});

export const { setPlaylist, setContentDetail } = playlistSlice.actions;
export default playlistSlice.reducer;

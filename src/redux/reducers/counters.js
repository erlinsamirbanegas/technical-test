import { createSlice } from "@reduxjs/toolkit";

export const accountersSlice = createSlice({
  name: "counters",
  initialState: {
    counters: [],
  },
  reducers: {
    setCounter: (state, action) => {
      state.counters.push(action.payload);
    },

    setIncrement: (state, action) => {
      const { counters } = state;

      state.counters = counters.map((item) => {
        if (item.id === action.payload.id) {
          item.value = item.value + action.payload.value;
          return item;
        }

        return item;
      });
    },

    setDecrease: (state, action) => {
      const { counters } = state;

      state.counters = counters.map((item) => {
        if (item.id === action.payload.id) {
          item.value = item.value - action.payload.value;
          return item;
        }

        return item;
      });
    },
  },
});

export const { setCounter, setIncrement, setDecrease } =
  accountersSlice.actions;
export default accountersSlice.reducer;
